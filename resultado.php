    <!DOCTYPE html>
    <html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Resultados</title>           
        <link rel="stylesheet" href="/proyectoquizrodrigovelasquez/css/resultado.css">

    </head>
    <body>
        <video autoplay muted loop id="video-background">
                <source src="img/fondo.mp4" type="video/mp4"> 
        </video>

    <?php
    session_start();
    $puntos_totales = 0;
    for ($i = 1; $i <= 14; $i++) {
        $pregunta_key = "pregunta" . $i;
        if (isset($_SESSION[$pregunta_key])) {
            $puntos_totales += $_SESSION[$pregunta_key];
        }
    }
    
    echo "<p class='texto'>Puntos totales: $puntos_totales puntos</p>";

    if ($puntos_totales < 15) {
        echo '<div class="circulo"><img src="/proyectoquizrodrigovelasquez/cantantes/drake.png"></div>';
        echo '<audio controls>';
        echo '<source src="img/Drake - Hotline Bling.mp3" type="audio/mpeg">';
        echo '</audio>';
    } elseif ($puntos_totales <= 15 && $puntos_totales < 20) {
        echo '<div class="circulo"><img src="/proyectoquizrodrigovelasquez/cantantes/davilio.png"></div>';
        echo '<audio controls>';
        echo  '<source src="img/Virgen.mp3" type="audio/mpeg">';
        echo '</audio>';
    } elseif ($puntos_totales <= 20 && $puntos_totales < 30) {
        echo '<div class="circulo"><img src="/proyectoquizrodrigovelasquez/cantantes/axel.png"></div>';
        echo '<audio controls>';
        echo  '<source src="img/ Paradise City Music .mp3" type="audio/mpeg">';
        echo '</audio>';
    } elseif ($puntos_totales <= 30 && $puntos_totales < 35) {
        echo '<div class="circulo"><img src="/proyectoquizrodrigovelasquez/cantantes/freddie.png"></div>';
        echo '<audio controls>';
        echo  '<source src="img/Queen – Bohemian Rhapsody (Official Video Remastered).mp3" type="audio/mpeg">';
        echo '</audio>';
    } elseif ($puntos_totales <= 35 && $puntos_totales < 40) {
        echo '<div class="circulo"><img src="/proyectoquizrodrigovelasquez/cantantes/iglesias.png"></div>';
        echo '<audio controls>';
        echo  '<source src="img/Enrique Iglesias - Bailando ft. Descemer Bueno, Gente De Zona (Letra).mp3" type="audio/mpeg">';
        echo '</audio>';
    } elseif ($puntos_totales <= 40 && $puntos_totales < 45) {
        echo '<div class="circulo"><img src="/proyectoquizrodrigovelasquez/cantantes/justin.png"></div>';
        echo '<audio controls>';
        echo  '<source src="img/Justin Bieber - Never Say Never ft. Jaden.mp3" type="audio/mpeg">';
        echo '</audio>';
    } elseif ($puntos_totales <= 45 && $puntos_totales < 50) {
        echo '<div class="circulo"><img src="/proyectoquizrodrigovelasquez/cantantes/harry.png"></div>';
        echo '<audio controls>';
        echo  '<source src="img/Harry Styles - As It Was (Official Video).mp3" type="audio/mpeg">';
        echo '</audio>';
    } elseif ($puntos_totales <= 50 && $puntos_totales < 55) {
        echo '<div class="circulo"><img src="/proyectoquizrodrigovelasquez/cantantes/pedro.png"></div>';
        echo '<audio controls>';
        echo  '<source src="img/Pedro Suarez Vertiz - Los globos del cielo (Video Oficial).mp3" type="audio/mpeg">';
        echo '</audio>';
    } elseif ($puntos_totales <= 55 && $puntos_totales < 60) {
        echo '<div class="circulo"><img src="/proyectoquizrodrigovelasquez/cantantes/shakira.png"></div>';
        echo '<audio controls>';
        echo  '<source src="img/Shakira.mp3" type="audio/mpeg">';
        echo '</audio>';
    } elseif ($puntos_totales <= 60 && $puntos_totales < 65) {
        echo '<div class="circulo"><img src="/proyectoquizrodrigovelasquez/cantantes/phili.png"></div>';
        echo '<audio controls>';
        echo  '<source src="img/Phillip Phillips - Gone, Gone, Gone.mp3" type="audio/mpeg">';
        echo '</audio>';
    } elseif ($puntos_totales <= 65 && $puntos_totales < 70) {
        echo '<div class="circulo"><img src="/proyectoquizrodrigovelasquez/cantantes/romeo.png"></div>';
        echo '<audio controls>';
        echo  '<source src="img/Aventura - Amor De Madre.mp3" type="audio/mpeg">';
        echo '</audio>';
    } elseif ($puntos_totales <= 70 && $puntos_totales < 75) {
        echo '<div class="circulo"><img src="/proyectoquizrodrigovelasquez/cantantes/michaeljackson.png"></div>';
        echo '<audio controls>';
        echo  '<source src="img/Michael Jackson - Beat It (Official 4K Video).mp3" type="audio/mpeg">';
        echo '</audio>';
    } elseif ($puntos_totales <= 75 && $puntos_totales < 80) {
        echo '<div class="circulo"><img src="/proyectoquizrodrigovelasquez/cantantes/JorgeLuisBonilla.png"></div>';
        echo '<audio controls>';
        echo  '<source src="img/Afrodisíaco - El Tiempo -- Letra.mp3" type="audio/mpeg">';
        echo '</audio>';
    } elseif ($puntos_totales <= 80 && $puntos_totales < 85) {
        echo '<div class="circulo"><img src="/proyectoquizrodrigovelasquez/cantantes/taylor.png"></div>';
    }
    ?>


    </body>
    </html> 