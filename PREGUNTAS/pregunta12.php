<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>juegoTest</title>
    <link rel="stylesheet" href="\proyectoquizrodrigovelasquez\css\preguntas3.css">
</head>
<body>
<?php
    $puntos11 = 0;
    session_start();
    if(isset($_POST["pregunta11"])){
        $opcion=$_POST["pregunta11"];
        switch($opcion){
            case "A":
                $puntos11 = $puntos11+6;
                break;
            case "B":
                $puntos11 = $puntos11+3;
                break;
            case "C":
                $puntos11 = $puntos11+1;
                break;
                
            case "D":
                $puntos11 = $puntos11+2;
                break;
            default:
                $puntos11 = 0;
                break;
        }
        $_SESSION["pregunta11"] = $puntos11;

    }
    ?>    
    <form action="\proyectoquizrodrigovelasquez\PREGUNTAS\pregunta13.php"  method="post">
        <div class="colocar">
            <div class="caja">
            <p>12.-: ¿Cuál es esa canción que siempre te anima cuando estás decaído?</p>
        <label class="ed">
            <input type="radio" name="pregunta12" value="A">
            "Happy" - Pharrell Williams<br>
            <input type="radio" name="pregunta12" value="B">
            "Eye of the Tiger" - Survivor <br>
            <input type="radio" name="pregunta12" value="C">
            "Don't Stop Believin'" - Journey<br>
            <input type="radio" name="pregunta12" value="D">
            "Can't Stop the Feeling!" - Justin Timberlake<br><br>
            <button type="submit" name="visitas">Siguiente</button> 
        </label>
        </div>  
        </div>
    </form>

</body>
</html>