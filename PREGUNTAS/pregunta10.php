<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>juegoTest</title>
    <link rel="stylesheet" href="\proyectoquizrodrigovelasquez\css\preguntas2.css">
</head>
<body>
<?php
    $puntos9 = 0;
    session_start();
    if(isset($_POST["pregunta9"])){
        $opcion=$_POST["pregunta9"];
        switch($opcion){
            case "A":
                $puntos9 = $puntos9+6;
                break;
            case "B":
                $puntos9 = $puntos9+3;
                break;
            case "C":
                $puntos9 = $puntos9+1;
                break;
                
            case "D":
                $puntos9 = $puntos9+2;
                break;
            default:
                $puntos9 = 0;
                break;
        }
        $_SESSION["pregunta9"] = $puntos9;

    }
    ?>    
    <form action="\proyectoquizrodrigovelasquez\PREGUNTAS\pregunta11.php"  method="post">
        <div class="colocar">
            <div class="caja">
            <p>10.-¿Qué te hace disfrutar más de una actuación en vivo?</p>
        <label class="ed">
            <input type="radio" name="pregunta10" value="A">
            La energía y la interacción con la audiencia <br>
            <input type="radio" name="pregunta10" value="B">
            La habilidad técnica y la destreza de los músicos <br>
            <input type="radio" name="pregunta10" value="C">
            La creatividad y la improvisación durante la presentación<br>
            <input type="radio" name="pregunta10" value="D">
            La conexión emocional y la interpretación del artista<br><br>
            <button type="submit" name="visitas">Siguiente</button> 
        </label>
        </div>  
        </div>
    </form>

</body>
</html>