<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>juegoTest</title>
    <link rel="stylesheet" href="\proyectoquizrodrigovelasquez\css\preguntas2.css">
</head>
<body>
<?php
    $puntos5 = 0;
    session_start();
    if(isset($_POST["pregunta5"])){
        $opcion=$_POST["pregunta5"];
        switch($opcion){
            case "A":
                $puntos5 = $puntos5+6;
                break;
            case "B":
                $puntos5 = $puntos5+3;
                break;
            case "C":
                $puntos5 = $puntos5+1;
                break;
            default:
                $puntos5 = 0;
                break;
        }
        $_SESSION["pregunta5"] = $puntos5;

    }

    ?>  
    <form action="\proyectoquizrodrigovelasquez\PREGUNTAS\pregunta7.php"  method="post">
        <div class="colocar">
            <div class="caja">
            <p>6.-¿Qué te hace conectar más con una canción?</p>
        <label class="ed">
            <input type="radio" name="pregunta6" value="A">
            El ritmo y la melodía <br>
            <input type="radio" name="pregunta6" value="B">
            Las letras y la poesía <br>
            <input type="radio" name="pregunta6" value="C">
            La instrumentación y los arreglos<br><br>
            <button type="submit" name="visitas">Siguiente</button> 
        </label>
        </div>  
        </div>
    </form>

</body>
</html>