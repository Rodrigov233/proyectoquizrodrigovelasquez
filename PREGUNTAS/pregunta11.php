<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>juegoTest</title>
    <link rel="stylesheet" href="\proyectoquizrodrigovelasquez\css\preguntas2.css">
</head>
<body>
<?php
    $puntos10 = 0;
    session_start();
    if(isset($_POST["pregunta10"])){
        $opcion=$_POST["pregunta10"];
        switch($opcion){
            case "A":
                $puntos10 = $puntos10+6;
                break;
            case "B":
                $puntos10 = $puntos10+3;
                break;
            case "C":
                $puntos10 = $puntos10+1;
                break;
                
            case "D":
                $puntos10 = $puntos10+2;
                break;
            default:
                $puntos10 = 0;
                break;
        }
        $_SESSION["pregunta10"] = $puntos10;

    }
    ?>    
    <form action="\proyectoquizrodrigovelasquez\PREGUNTAS\pregunta12.php"  method="post">
        <div class="colocar">
            <div class="caja">
            <p>11.-¿Cuál ha sido tu mejor experiencia en un concierto o festival de música?</p> 
        <label class="ed">
            <input type="radio" name="pregunta11" value="A">
            Ver a mi banda favorita en primera fila y sentir la energía del público.<br>
            <input type="radio" name="pregunta11" value="B">
            Bailar toda la noche en un festival al aire libre con mis amigos.<br>
            <input type="radio" name="pregunta11" value="C">
            Descubrir nuevos artistas en un concierto íntimo y vibrante.<br>
            <input type="radio" name="pregunta11" value="D">
            Cantar junto a miles de personas en un concierto inolvidable.<br><br>
            <button type="submit" name="visitas">Siguiente</button> 
        </label>
        </div>  
        </div>
    </form>

</body>
</html>