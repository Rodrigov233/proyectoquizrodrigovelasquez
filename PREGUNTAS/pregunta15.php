<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>juegoTest</title>
    <link rel="stylesheet" href="\proyectoquizrodrigovelasquez\css\preguntas2.css">
</head>
<body>
<?php
    $puntos14 = 0;
    session_start();
    if(isset($_POST["pregunta14"])){
        $opcion=$_POST["pregunta14"];
        switch($opcion){
            case "A":
                $puntos14 = $puntos14+6;
                break;
            case "B":
                $puntos14 = $puntos14+3;
                break;
            case "C":
                $puntos14 = $puntos14+1;
                break;
                
            case "D":
                $puntos14 = $puntos14+2;
                break;
            default:
                $puntos14 = 0;
                break;
        }
        $_SESSION["pregunta14"] = $puntos14;

    }
    ?>    
        <form action="\proyectoquizrodrigovelasquez\resultado.php"  method="post">
        <div class="colocar">
            <div class="caja">
            <p>15.-¿Qué prefieres en una canción: la letra o la melodía?</p>
        <label class="ed">
            <input type="radio" name="pregunta15" value="A">
            Letra, me gusta profundizar en el significado de las letras.<br>
            <input type="radio" name="pregunta15" value="B">
            Melodía, me encanta cómo una buena melodía puede emocionarme.<br>
            <input type="radio" name="pregunta15" value="C">
            Ambas, creo que tanto la letra como la melodía son importantes por igual.<br>
            <input type="radio" name="pregunta15" value="D">
            Depende del estado de ánimo y la situación.<br><br>
            <button type="submit" name="visitas">Siguiente</button> 
        </label>
        </div>  
        </div>
    </form>

</body>
</html>