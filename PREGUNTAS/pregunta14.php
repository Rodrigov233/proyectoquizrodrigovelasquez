<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>juegoTest</title>
    <link rel="stylesheet" href="\proyectoquizrodrigovelasquez\css\preguntas1.css">
</head>
<body>
<?php
    $puntos13 = 0;
    session_start();
    if(isset($_POST["pregunta13"])){
        $opcion=$_POST["pregunta13"];
        switch($opcion){
            case "A":
                $puntos13 = $puntos13+6;
                break;
            case "B":
                $puntos13 = $puntos13+3;
                break;
            case "C":
                $puntos13 = $puntos13+1;
                break;
                
            case "D":
                $puntos13 = $puntos13+2;
                break;
                default:
                $puntos13 = 0;
                break;
        }
        $_SESSION["pregunta13"] = $puntos13;
    }
    ?>    
    <form action="\proyectoquizrodrigovelasquez\PREGUNTAS\pregunta15.php"  method="post">
        <div class="colocar">
            <div class="caja">
            <p>14.-¿Crees que escuchar música mientras trabajas o estudias aumenta tu productividad?</p>
        <label class="ed">
            <input type="radio" name="pregunta14" value="A">
            : Sí, la música me ayuda a concentrarme y a ser más productivo/a. <br>
            <input type="radio" name="pregunta14" value="B">
            No, encuentro que la música me distrae y disminuye mi productividad.<br>
            <input type="radio" name="pregunta14" value="C">
            Depende, a veces la música me ayuda y otras veces me distrae. <br>
            <input type="radio" name="pregunta14" value="D">
            No estoy seguro/a, no suelo escuchar música mientras trabajo o estudio.<br><br>
            <button type="submit" name="visitas">Siguiente</button> 
        </label>
        </div>  
        </div>
    </form>

</body>
</html>