<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>juegoTest</title>
    <link rel="stylesheet" href="\proyectoquizrodrigovelasquez\css\preguntas4.css">
</head>
<body>
<?php
    $puntos7 = 0;
    session_start();
    if(isset($_POST["pregunta7"])){
        $opcion=$_POST["pregunta7"];
        switch($opcion){
            case "A":
                $puntos7 = $puntos7+6;
                break;
            case "B":
                $puntos7 = $puntos7+3;
                break;
            case "C":
                $puntos7 = $puntos7+1;
                break;
            default:
                $puntos7 = 0;
                break;
        }
        $_SESSION["pregunta7"] = $puntos7;
    }
    ?>    
    <form action="\proyectoquizrodrigovelasquez\PREGUNTAS\pregunta9.php"  method="post">
        <div class="colocar">
        <div class="caja">
            <p>8.-¿Qué te inspira más a descubrir nueva música?</p>
        <label class="ed">
            <input type="radio" name="pregunta8" value="A">
            Recomendaciones de amigos o familiares
            <br>
            <input type="radio" name="pregunta8" value="B">
            Listas de reproducción personalizadas en plataformas de streaming
            <br>
            <input type="radio" name="pregunta8" value="C">
            Descubrimiento aleatorio mientras exploras en línea <br><br>
            <button type="submit" name="visitas">Siguiente</button> 
        </label>
    </div>  
        </div>
    </form>

</body>
</html>