<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>juegoTest</title>
    <link rel="stylesheet" href="\proyectoquizrodrigovelasquez\css\preguntas1.css">
</head>
<body>
<?php
    $puntos8 = 0;
    session_start();
    if(isset($_POST["pregunta8"])){
        $opcion=$_POST["pregunta8"];
        switch($opcion){
            case "A":
                $puntos8 = $puntos8+6;
                break;
            case "B":
                $puntos8 = $puntos8+3;
                break;
            case "C":
                $puntos8 = $puntos8+1;
                break;
            default:
                $puntos8 = 0;
                break;
        }
        $_SESSION["pregunta8"] = $puntos8;
    }
    ?>    
    <form action="\proyectoquizrodrigovelasquez\PREGUNTAS\pregunta10.php"  method="post">
        <div class="colocar">
    <div class="caja">
        <p>9.-¿Qué influencia más tu elección de música para escuchar en determinados momentos?</p>
        <label class="ed">
            <input type="radio" name="pregunta9" value="A">
            Tu estado de ánimo o emociones
            <br>
            <input type="radio" name="pregunta9" value="B">
            El ambiente o lugar en el que te encuentras 
            <br>
            <input type="radio" name="pregunta9" value="C">
            El clima o la temporada del año <br><br>
            <button type="submit" name="visitas">Siguiente</button> 
        </label> 
    </div>
        </div>
    </form>

</body>
</html>