<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>juegoTest</title>
    <link rel="stylesheet" href="\proyectoquizrodrigovelasquez\css\preguntas4.css">
</head>
<body>
<?php
    $puntos12 = 0;
    session_start();
    if(isset($_POST["pregunta12"])){
        $opcion=$_POST["pregunta12"];
        switch($opcion){
            case "A":
                $puntos12 = $puntos12+6;
                break;
            case "B":
                $puntos12 = $puntos12+3;
                break;
            case "C":
                $puntos12 = $puntos12+1;
                break;
                
            case "D":
                $puntos12 = $puntos12+2;
                break;
            default:
                $puntos12 = 0;
                break;
        }
        $_SESSION["pregunta12"] = $puntos12;
    }
    ?>    
    <form action="\proyectoquizrodrigovelasquez\PREGUNTAS\pregunta14.php"  method="post">
        <div class="colocar">
            <div class="caja">
            <p>13.- ¿Tienes alguna canción que te transporte de inmediato a un momento específico de tu pasado?</p>
        <label class="ed">
            <input type="radio" name="pregunta13" value="A">
            Sí, hay una canción que siempre me lleva de vuelta a ese momento. <br>
            <input type="radio" name="pregunta13" value="B">
            No tengo una canción específica que haga eso.<br>
            <input type="radio" name="pregunta13" value="C">
            A veces, ciertas canciones pueden evocar recuerdos, pero no de manera consistente.<br>
            <input type="radio" name="pregunta13" value="D">
            No suelo asociar la música con recuerdos específicos.<br><br>
            <button type="submit" name="visitas">Siguiente</button> 
        </label>
        </div>  
        </div>
    </form>

</body>
</html>