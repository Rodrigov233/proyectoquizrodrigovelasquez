<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>juegoTest</title>
    <link rel="stylesheet" href="\proyectoquizrodrigovelasquez\css\preguntas4.css">
</head>
<body>
<?php
    $puntos3 = 0;
    session_start();
    if(isset($_POST["pregunta3"])){
        $opcion=$_POST["pregunta3"];
        switch($opcion){
            case "A":
                $puntos3 = $puntos3+6;
                break;
            case "B":
                $puntos3 = $puntos3+3;
                break;
            case "C":
                $puntos3 = $puntos3+1;
                break;
            default:
                $puntos3 = 0;
                break;
        }
        $_SESSION["pregunta3"] = $puntos3;


    }

    ?>    
    <form action="\proyectoquizrodrigovelasquez\PREGUNTAS\pregunta5.php"  method="post">
    <div class="colocar">
        <div class="caja">
        <p>4.-¿Qué instrumento musical te gusta más escuchar en una canción?</p>
        <label class="ed">
            <input type="radio" name="pregunta4" value="A">
            Guitarra eléctrica<br>
            <input type="radio" name="pregunta4" value="B">
            Batería <br>
            <input type="radio" name="pregunta4" value="C">
            Piano <br><br>
            <button type="submit" name="visitas">Siguiente</button> 
        </label> 
    </div>
    </div>
    </form>

</body>
</html>